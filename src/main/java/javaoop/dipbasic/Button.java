package javaoop.dipbasic;

public class Button {
    private final Lamp lamp;

    public Button(Lamp lamp) {
        this.lamp = lamp;
    }

    // this would get executed 30 times per second in a loop
    public void poll() {
        if (true /* if the physical-button is clicked */) {
            lamp.turnOn();
        } else {
            lamp.turnOff();
        }
    }
}
