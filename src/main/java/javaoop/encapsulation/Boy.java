package javaoop.encapsulation;

public class Boy {
    // use of final not applicable here like nowhere else :P
    private Girl girlfriend;

    public void introduceToHim(Girl girl) {
        System.out.println("hi");
    }

    public void liveSomeOfLifetime() {
        // blablabla
        // many things
        // ...
        girlfriend = new Girl();
        // and much many things
        // ...
    }
}
