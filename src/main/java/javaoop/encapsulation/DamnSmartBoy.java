package javaoop.encapsulation;

public class DamnSmartBoy extends Boy {
    @Override
    public void introduceToHim(Girl girl) {
        girl.boyfriend = this;
    }
}
