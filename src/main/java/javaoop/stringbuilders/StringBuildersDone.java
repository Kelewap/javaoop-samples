package javaoop.stringbuilders;

import java.util.Arrays;
import java.util.List;

public class StringBuildersDone {
    public static void main(String[] args) {
        List<String> employees = Arrays.asList("John", "Steve", "Boris", "Vladimir");

        String niceString = composeNiceEmployeesString(employees);
        System.out.println(niceString);

        niceString = composeNiceEmployeesStringWithStringBuilder(employees);
        System.out.println(niceString);

        niceString = composeNiceEmployeesStringWithJoin(employees);
        System.out.println(niceString);
    }

    // not efficient nor concise
    private static String composeNiceEmployeesString(List<String> employees) {
        String niceEmployeesString = "My employees are: ";

        for (String employee : employees) {
            niceEmployeesString += employee;
            niceEmployeesString += ", ";
        }

        return niceEmployeesString;
    }

    // efficient but not concise
    private static String composeNiceEmployeesStringWithStringBuilder(List<String> employees) {
        StringBuilder stringBuilder = new StringBuilder("My employees are: ");

        for (String employee : employees) {
            stringBuilder.append(employee);
            stringBuilder.append(", ");
        }

        return stringBuilder.toString();
    }

    // efficient and concise
    private static String composeNiceEmployeesStringWithJoin(List<String> employees) {
        return "My employees are: " + String.join(", ", employees);
    }
}
